var expect = require('chai').expect,
    path = require('path');

var Rectangle = require(path.join(__dirname, '../lib', 'rectangle'));

describe('Rectangle', () => {

    describe('#width', () => {
        var rectangle;

        beforeEach(() => {
            rectangle = new Rectangle(10, 20);
        });

        it('returns the width', () => {
            expect(rectangle.getWidth()).to.be.equal(10);
        });

        it('can be changed', () => {
            rectangle.setWidth(30);
            expect(rectangle.getWidth()).to.be.equal(30);
        });

        it('only accepts numerical values', () => {
            expect(
                () => {
                    rectangle.setWidth('foo');
                }).to.throw('"width" must be a number.');
        });
    });

    describe('#height', () => {
        beforeEach(() => {
            rectangle = new Rectangle(10, 20);
        });

        it('returns the height', () => {
            expect(rectangle.getHeight()).to.be.equal(20);
        });

        it('can be changed', () => {
            rectangle.setHeight(25);
            expect(rectangle.getHeight()).to.be.equal(25);
        });

        it('only accepts numerical values', () => {
            expect(
                () => {
                    rectangle.setHeight('foo');
                }).to.throw('"height" must be a number.');
        });
    });

    describe('#operations', () => {
        beforeEach(() => {
            rectangle = new Rectangle(10, 20);
        });

        it('returns the area', () => {
            expect(rectangle.getArea()).to.be.equal(200);
        });

        it('returns the circunference', () => {
            expect(rectangle.getCircumference()).to.be.equal(60);
        });
    });
});
