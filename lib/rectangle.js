function Rectangle(width, height) {
    this.width = width;
    this.height = height;

    this.getWidth = () => {
        return this.width;
    }

    this.setWidth = (value) => {
        if (typeof value !== 'number') {
            throw new Error('"width" must be a number.');
        }

        this.width = value;
    }

    this.getHeight = () => {
      return this.height;
    }

    this.setHeight = (value) => {
      if (typeof value !== 'number') {
        throw new Error('"height" must be a number.');
      }

      this.height = value;
    }

    this.getArea = () => {
        return this.width * this.height;
    }

    this.getCircumference = () => {
        return 2 * this.width + 2 * this.height;
    }
}

module.exports = Rectangle;
